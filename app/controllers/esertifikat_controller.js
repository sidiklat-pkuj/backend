const { where } = require("sequelize");
const db = require("../models");
const esertifikat = db.esertifikat;

exports.get=(req,res)=>{
    esertifikat.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getBySubMenu=(req,res)=>{
    esertifikat.findAll({
        where:{subMenuId:req.params.submenuid}
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    esertifikat.create({
        nama:req.body.nama,
        file:req.file.filename,
        subMenuId:req.body.subMenuId
        
    })
     .then(esertifikat=>{
        res.send(esertifikat);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    req.body.file = req.file.filename
    esertifikat.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    esertifikat.destroy({where:{id:id}})
    .then(res.send({message:"Delete esertifikat berhasil"}))
};