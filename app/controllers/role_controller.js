const db = require("../models");
const Role = db.role;

exports.get=(req,res)=>{
    Role.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    Role.create({
        name:req.body.name
    })
     .then(role=>{
        res.send(role);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    Role.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    Role.destroy({where:{id:id}})
    .then(res.send({message:"Delete role berhasil"}))
};