const db = require("../models");
const menu = db.menu;
const submenu = db.submenu;
const master_jenis = db.master_jenis;

exports.get=(req,res)=>{
    menu.findAll({
        include: [
            {
            model:submenu
        },
        {
            model:master_jenis
        }
    ]
    })
    .then(data=>{
        res.send(data);
    });
};

exports.getTahunPelatihan=(req,res)=>{
    menu.findAll({
        where:{masterJenisId:null},
        include: [
            {
            model:submenu
        },
        {
            model:master_jenis
        }
    ]
    })
    .then(data=>{
        res.send(data);
    });
};

exports.getByJenis=(req,res)=>{
    menu.findAll({
        where:{
            masterJenisId:req.params.jenis
        },
        include: [
            {
            model:submenu
        },
        {
            model:master_jenis
        }
    ]
    })
    .then(data=>{
        res.send(data);
    });
};

exports.getOne=(req,res)=>{
    menu.findOne({
        where:{
            id:req.params.id
        },
        include: [
            {
                model:submenu
            },
            {
                model:master_jenis
            }
        ]
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    menu.create({
        nama:req.body.nama,
        urutan:req.body.urutan,
        masterJenisId:req.body.masterJenisId,
    })
     .then(menu=>{
        res.send(menu);
     });
};

exports.edit=(req,res)=>{
    console.log(req.body);
    console.log(req.params.id);
    console.log("req.body =======>");
    const id= req.params.id;
    menu.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    menu.destroy({where:{id:id}})
    .then(res.send({message:"Delete menu berhasil"}))
};