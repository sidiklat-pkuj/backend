const db = require("../models");
const menu = db.menu;
const master_jenis = db.master_jenis;

exports.get=(req,res)=>{
    master_jenis.findAll({
        include: {
            model:menu
        }
    })
    .then(data=>{
        res.send(data);
    });
};

exports.getOne=(req,res)=>{
    master_jenis.findOne({
        where:{
            id:req.params.id
        },
        include: {
            model:menu
        }
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    master_jenis.create({
        nama:req.body.nama,
        
    })
     .then(menu=>{
        res.send(menu);
     });
};

exports.edit=(req,res)=>{
    console.log(req.body);
    console.log(req.params.id);
    console.log("req.body =======>");
    const id= req.params.id;
    master_jenis.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    master_jenis.destroy({where:{id:id}})
    .then(res.send({message:"Delete menu berhasil"}))
};