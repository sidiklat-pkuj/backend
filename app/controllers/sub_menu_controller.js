const db = require("../models");
const subMenu = db.submenu;
const peserta = db.peserta;
const narasumber = db.narasumber;

exports.get=(req,res)=>{
    subMenu.findAll({
        include: [
            {
                model:narasumber
            },{
                model:peserta
            }
        ]
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    subMenu.create({
        nama:req.body.nama,
        urutan:req.body.urutan,
        tanggal_pelaksanaan:req.body.tanggal_pelaksanaan,
        menuId:req.body.menuId,
    })
     .then(subMenu=>{
        res.send(subMenu);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    subMenu.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    subMenu.destroy({where:{id:id}})
    .then(res.send({message:"Delete subMenu berhasil"}))
};