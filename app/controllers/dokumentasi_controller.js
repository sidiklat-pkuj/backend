const { where } = require("sequelize");
const db = require("../models");
const dokumentasi = db.dokumentasi;

exports.get=(req,res)=>{
    dokumentasi.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getBySubMenu=(req,res)=>{
    dokumentasi.findAll({
        where:{subMenuId:req.params.submenuid}
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    dokumentasi.create({
        nama:req.body.nama,
        file:req.file.filename,
        subMenuId:req.body.subMenuId
        
    })
     .then(dokumentasi=>{
        res.send(dokumentasi);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    req.body.file = req.file.filename
    dokumentasi.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    dokumentasi.destroy({where:{id:id}})
    .then(res.send({message:"Delete dokumentasi berhasil"}))
};