const { where } = require("sequelize");
const db = require("../models");
const mot = db.mot;

exports.get=(req,res)=>{
    mot.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getBySubMenu=(req,res)=>{
    mot.findAll({
        where:{subMenuId:req.params.submenuid}
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    mot.create({
        nip:req.body.nip,
        nama:req.body.nama,
        nik:req.body.nik,
        jenis_pegawai:req.body.jenis_pegawai,
        asal_instansi:req.body.asal_instansi,
        status_pegawai:req.body.status_pegawai,
        tempat_lahir:req.body.tempat_lahir,
        tgl_lahir:req.body.tgl_lahir,
        pendidikan_terakhir:req.body.pendidikan_terakhir,
        jurusan_pendidikan:req.body.jurusan_pendidikan,
        jenis_kelamin:req.body.jenis_kelamin,
        agama:req.body.agama,
        gol:req.body.gol,
        provinsi_instansi:req.body.provinsi_instansi,
        alamat_kantor:req.body.alamat_kantor,
        alamat_rumah:req.body.alamat_rumah,
        nomor_hp:req.body.nomor_hp,
        email:req.body.email,
        profesi:req.body.profesi,
        profesi_lainnya:req.body.profesi_lainnya,
        jabatan:req.body.jabatan,
        pengalaman_mengajar:req.body.pengalaman_mengajar,
        pengalaman_kerja:req.body.pengalaman_kerja,
        nira:req.body.nira,
        no_npwp:req.body.no_npwp,
        sertif_pelatihan:req.body.sertif_pelatihan,
        sertif_tot:req.body.sertif_tot,
        sertif_pp:req.body.sertif_pp,
        str_aktif:req.body.str_aktif,
        ijazah_terakhir:req.body.ijazah_terakhir,
        pas_foto:req.file.filename,
        bidang_keahlian:req.body.bidang_keahlian,
        subMenuId:req.body.subMenuId,
    })
     .then(mot=>{
        res.send(mot);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    req.body.pas_foto = req.file.filename
    mot.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    mot.destroy({where:{id:id}})
    .then(res.send({message:"Delete mot berhasil"}))
};