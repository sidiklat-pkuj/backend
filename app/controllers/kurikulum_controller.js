const { where } = require("sequelize");
const db = require("../models");
const kurikulum = db.kurikulum;

exports.get=(req,res)=>{
    kurikulum.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getBySubMenu=(req,res)=>{
    kurikulum.findAll({
        where:{subMenuId:req.params.submenuid}
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    kurikulum.create({
        nama:req.body.nama,
        file:req.file.filename,
        subMenuId:req.body.subMenuId
        
    })
     .then(kurikulum=>{
        res.send(kurikulum);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    req.body.file = req.file.filename
    kurikulum.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    kurikulum.destroy({where:{id:id}})
    .then(res.send({message:"Delete kurikulum berhasil"}))
};