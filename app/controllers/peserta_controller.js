const db = require("../models");
const Peserta = db.peserta;

exports.get=(req,res)=>{
    Peserta.findAll()
    .then(data=>{
        res.send(data);
    });
};

// exports.getByCreated=(req,res)=>{
//     const id = req.params.id;
//     Peserta.findAll({
//         where: {judulUndianId:id},
//         include: Undian
//     })
//     .then(data=>{
//         res.send(data);
//     });
// };

exports.getBySubMenu=(req,res)=>{
    Peserta.findAll({
        where:{subMenuId:req.params.submenuid}
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    // console.log(req);
    console.log(req.body);
    console.log("req =============>");
    Peserta.create({
        no_ktp:req.body.no_ktp,
        link_foto:req.body.link_foto,
        tipe_pegawai:req.body.tipe_pegawai,
        nip:req.body.nip,
        no_str:req.body.no_str,
        gelar_depan:req.body.gelar_depan,
        nama_lengkap:req.body.nama_lengkap,
        gelar_belakang:req.body.gelar_belakang,
        jenis_kelamin:req.body.jenis_kelamin,
        tempat_lahir:req.body.tempat_lahir,
        tgl_lahir:req.body.tgl_lahir,
        agama:req.body.agama,
        pendidikan_terakhir:req.body.pendidikan_terakhir,
        pangkat:req.body.pangkat,
        nama_instansi:req.body.nama_instansi,
        alamat_instansi:req.body.alamat_instansi,
        kab_instansi:req.body.kab_instansi,
        prov_instansi:req.body.prov_instansi,
        alamat_rumah:req.body.alamat_rumah,
        kab_rumah:req.body.kab_rumah,
        prov_rumah:req.body.prov_rumah,
        no_hp:req.body.no_hp,
        email:req.body.email,
        jenis_nakes:req.body.jenis_nakes,
        jabatan:req.body.jabatan,
        subMenuId:req.body.subMenuId,

    })
     .then(peserta=>{
        res.send(peserta);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    Peserta.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    Peserta.destroy({where:{id:id}})
    .then(res.send({message:"Delete peserta berhasil"}))
};