const { where } = require("sequelize");
const db = require("../models");
const keuangan = db.keuangan;

exports.get=(req,res)=>{
    keuangan.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getByMenu=(req,res)=>{
    keuangan.findAll({
        where:{menuId:req.params.menuid}
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    keuangan.create({
        nama:req.body.nama,
        file:req.file.filename,
        uang_masuk:req.body.uang_masuk,
        uang_keluar:req.body.uang_keluar,
        tahun:req.body.tahun,
        tanggal:req.body.tanggal,
        menuId:req.body.menuId
        
    })
     .then(keuangan=>{
        res.send(keuangan);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    req.body.file = req.file.filename
    keuangan.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    keuangan.destroy({where:{id:id}})
    .then(res.send({message:"Delete keuangan berhasil"}))
};