const { where } = require("sequelize");
const db = require("../models");
const surat = db.surat;

exports.get=(req,res)=>{
    surat.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getByMenu=(req,res)=>{
    surat.findAll({
        where:{menuId:req.params.menuid}
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    surat.create({
        nama:req.body.nama,
        file:req.file.filename,
        menuId:req.body.menuId,
        tanggal_masehi:req.body.tanggal_masehi,
          tanggal_hijriyah:req.body.tanggal_hijriyah,
          nomor_surat:req.body.nomor_surat,
          unit_kerja:req.body.unit_kerja,
          instansi:req.body.instansi,
          keterangan:req.body.keterangan
        
    })
     .then(surat=>{
        res.send(surat);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    req.body.file = req.file.filename
    surat.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    surat.destroy({where:{id:id}})
    .then(res.send({message:"Delete surat berhasil"}))
};