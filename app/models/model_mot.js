module.exports = (sequelize, Sequelize) => {
    const mot = sequelize.define("mot", {
      nip: {
        type: Sequelize.STRING
      },
      nama:{
        type: Sequelize.STRING
      },
      nik:{
        type: Sequelize.STRING
      },
      jenis_pegawai: {
        type: Sequelize.STRING
      },
      asal_instansi: {
        type: Sequelize.STRING
      },
      status_pegawai: {
        type: Sequelize.STRING
      },
      tempat_lahir: {
        type: Sequelize.STRING
      },
      tgl_lahir: {
        type: Sequelize.STRING
      },
      pendidikan_terakhir: {
        type: Sequelize.STRING
      },
      jurusan_pendidikan: {
        type: Sequelize.STRING
      },
      jenis_kelamin: {
        type: Sequelize.STRING
      },
      agama: {
        type: Sequelize.STRING
      },
      gol: {
        type: Sequelize.STRING
      },
      provinsi_instansi: {
        type: Sequelize.STRING
      },
      alamat_kantor: {
        type: Sequelize.STRING
      },
      alamat_rumah: {
        type: Sequelize.STRING
      },
      nomor_hp: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      profesi: {
        type: Sequelize.STRING
      },
      profesi_lainnya: {
        type: Sequelize.STRING
      },
      jabatan: {
        type: Sequelize.STRING
      },
      pengalaman_mengajar: {
        type: Sequelize.STRING
      },
      pengalaman_kerja: {
        type: Sequelize.STRING
      },
      nira: {
        type: Sequelize.STRING
      },
      no_npwp: {
        type: Sequelize.STRING
      },
      sertif_pelatihan: {
        type: Sequelize.STRING
      },
      sertif_tot: {
        type: Sequelize.STRING
      },
      sertif_pp: {
        type: Sequelize.STRING
      },
      str_aktif: {
        type: Sequelize.STRING
      },
      ijazah_terakhir: {
        type: Sequelize.STRING
      },
      pas_foto: {
        type: Sequelize.STRING
      },
      bidang_keahlian: {
        type: Sequelize.STRING
      },
    });
  
    return mot;
  };
  