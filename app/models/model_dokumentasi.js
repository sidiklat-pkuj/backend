module.exports = (sequelize, Sequelize) => {
    const dokumentasi = sequelize.define("dokumentasi", {
      nama:{
        type: Sequelize.STRING
      },
      file:{
        type: Sequelize.STRING
      },
    });
  
    return dokumentasi;
  };
  