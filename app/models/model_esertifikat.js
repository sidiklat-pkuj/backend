module.exports = (sequelize, Sequelize) => {
    const esertifikat = sequelize.define("esertifikat", {
      nama:{
        type: Sequelize.STRING
      },
      file:{
        type: Sequelize.STRING
      },
    });
  
    return esertifikat;
  };
  