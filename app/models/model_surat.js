module.exports = (sequelize, Sequelize) => {
    const surat = sequelize.define("surat", {
      nama:{
        type: Sequelize.STRING
      },
      tanggal_masehi:{
        type: Sequelize.STRING
      },
      tanggal_hijriyah:{
        type: Sequelize.STRING
      },
      nomor_surat:{
        type: Sequelize.STRING
      },
      unit_kerja:{
        type: Sequelize.STRING
      },
      instansi:{
        type: Sequelize.STRING
      },
      keterangan:{
        type: Sequelize.STRING
      },
      file:{
        type: Sequelize.STRING
      }
    });
  
    return surat;
  };
  