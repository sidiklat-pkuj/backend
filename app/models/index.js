const config = require("../config/config_db.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,
    {
        host: config.HOST,
        dialect: config.dialect,
        operatorsAliases: false,

        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        }
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./model_user.js")(sequelize, Sequelize);
db.role = require("./model_role.js")(sequelize, Sequelize);
db.peserta = require("./model_peserta.js")(sequelize, Sequelize);
db.menu = require("./model_menu.js")(sequelize, Sequelize);
db.submenu = require("./model_submenu.js")(sequelize, Sequelize);
db.narasumber = require("./model_narasumber.js")(sequelize, Sequelize);
db.kurikulum = require("./model_kurikulum.js")(sequelize, Sequelize);
db.mot = require("./model_mot.js")(sequelize, Sequelize);
db.dokumentasi = require("./model_dokumentasi.js")(sequelize, Sequelize);
db.esertifikat = require("./model_esertifikat.js")(sequelize, Sequelize);
db.master_jenis = require("./model_master_jenis.js")(sequelize, Sequelize);
db.surat = require("./model_surat.js")(sequelize, Sequelize);
db.keuangan = require("./model_keuangan.js")(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
    through: "user_roles",
    foreignKey: "roleId",
    otherKey: "userId"
});
db.user.belongsToMany(db.role, {
    through: "user_roles",
    foreignKey: "userId",
    otherKey: "roleId"
});

db.peserta.hasOne(db.user, {foreignKey: "pesertaId"});
db.master_jenis.hasMany(db.menu, {foreignKey: "masterJenisId"});
db.menu.belongsTo(db.master_jenis, {foreignKey : 'masterJenisId'});

db.menu.hasMany(db.submenu, {foreignKey: "menuId"});
db.submenu.belongsTo(db.menu, {foreignKey : 'menuId'});

db.submenu.hasMany(db.narasumber, {foreignKey: "subMenuId"});
db.narasumber.belongsTo(db.submenu, {foreignKey : 'subMenuId'});

db.submenu.hasMany(db.peserta, {foreignKey: "subMenuId"});
db.peserta.belongsTo(db.submenu, {foreignKey : 'subMenuId'});

db.submenu.hasMany(db.kurikulum, {foreignKey: "subMenuId"});
db.kurikulum.belongsTo(db.submenu, {foreignKey : 'subMenuId'});

db.submenu.hasMany(db.mot, {foreignKey: "subMenuId"});
db.mot.belongsTo(db.submenu, {foreignKey : 'subMenuId'});

db.submenu.hasMany(db.dokumentasi, {foreignKey: "subMenuId"});
db.dokumentasi.belongsTo(db.submenu, {foreignKey : 'subMenuId'});

db.submenu.hasMany(db.esertifikat, {foreignKey: "subMenuId"});
db.esertifikat.belongsTo(db.submenu, {foreignKey : 'subMenuId'});

db.menu.hasMany(db.surat, {foreignKey: "menuId"});
db.surat.belongsTo(db.menu, {foreignKey : 'menuId'});

db.menu.hasMany(db.keuangan, {foreignKey: "menuId"});
db.keuangan.belongsTo(db.menu, {foreignKey : 'menuId'});
// db.user.belongsTo(db.peserta);

db.sequelize.sync({ alter: true })
db.ROLES = ["user", "admin"];

module.exports = db;