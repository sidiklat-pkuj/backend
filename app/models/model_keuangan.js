module.exports = (sequelize, Sequelize) => {
    const keuangan = sequelize.define("keuangan", {
      nama:{
        type: Sequelize.STRING
      },
      tanggal:{
        type: Sequelize.STRING
      },
      uang_masuk:{
        type: Sequelize.INTEGER
      },
      uang_keluar:{
        type: Sequelize.INTEGER
      },
      tahun:{
        type: Sequelize.INTEGER
      },
      file:{
        type: Sequelize.STRING
      }
    });
  
    return keuangan;
  };
  