module.exports = (sequelize, Sequelize) => {
    const Menu = sequelize.define("menu", {
      nama:{
        type: Sequelize.STRING
      },
      urutan:{
        type: Sequelize.INTEGER
      }
    });
  
    return Menu;
  };
  