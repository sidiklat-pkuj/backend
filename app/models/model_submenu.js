module.exports = (sequelize, Sequelize) => {
    const Submenu = sequelize.define("sub_menu", {
      nama:{
        type: Sequelize.STRING
      },
      tanggal_pelaksanaan:{
        type: Sequelize.STRING
      },
      urutan:{
        type: Sequelize.INTEGER
      }
    });
  
    return Submenu;
  };
  