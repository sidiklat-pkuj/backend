module.exports = (sequelize, Sequelize) => {
    const kurikulum = sequelize.define("kurikulum", {
      nama:{
        type: Sequelize.STRING
      },
      file:{
        type: Sequelize.STRING
      }
    });
  
    return kurikulum;
  };
  