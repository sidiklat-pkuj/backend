module.exports = (sequelize, Sequelize) => {
    const Peserta = sequelize.define("peserta", {
      no_ktp: {
        type: Sequelize.STRING
      },
      link_foto:{
        type: Sequelize.STRING
      },
      tipe_pegawai: {
        type: Sequelize.STRING
      },
      nip: {
        type: Sequelize.STRING
      },
      no_str: {
        type: Sequelize.STRING
      },
      nama_lengkap: {
        type: Sequelize.STRING
      },
      gelar_depan: {
        type: Sequelize.STRING
      },
      gelar_belakang: {
        type: Sequelize.STRING
      },
      jenis_kelamin: {
        type: Sequelize.STRING
      },
      tempat_lahir: {
        type: Sequelize.STRING
      },
      tgl_lahir: {
        type: Sequelize.STRING
      },
      agama: {
        type: Sequelize.STRING
      },
      pendidikan_terakhir: {
        type: Sequelize.STRING
      },
      pangkat: {
        type: Sequelize.STRING
      },
      nama_instansi: {
        type: Sequelize.STRING
      },
      alamat_instansi: {
        type: Sequelize.STRING
      },
      kab_instansi: {
        type: Sequelize.STRING
      },
      prov_instansi: {
        type: Sequelize.STRING
      },
      alamat_rumah: {
        type: Sequelize.STRING
      },
      kab_rumah: {
        type: Sequelize.STRING
      },
      prov_rumah: {
        type: Sequelize.STRING
      },
      no_hp: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      jenis_nakes: {
        type: Sequelize.STRING
      },
      jabatan: {
        type: Sequelize.STRING
      },
    });
  
    return Peserta;
  };
  