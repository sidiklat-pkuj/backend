const {authJwt} = require("../middleware");
const controller = require("../controllers/peserta_controller");

module.exports=function(app){
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.get("/api/peserta/:submenuid",[authJwt.verifyToken],controller.getBySubMenu);
    app.post("/api/peserta",[authJwt.verifyToken],controller.tambah);
    app.put("/api/peserta/:id",[authJwt.verifyToken],controller.edit);
    app.delete("/api/peserta/:id",[authJwt.verifyToken],controller.delete);
}