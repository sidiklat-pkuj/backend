const {authJwt} = require("../middleware");
const controller = require("../controllers/master_jenis_controller");

module.exports=function(app){
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.get("/api/masterjenis/",[authJwt.verifyToken],controller.get);
    app.get("/api/masterjenis/:id",[authJwt.verifyToken],controller.getOne);
    app.post("/api/masterjenis",[authJwt.verifyToken,authJwt.isAdmin],controller.tambah);
    app.put("/api/masterjenis/:id",[authJwt.verifyToken,authJwt.isAdmin],controller.edit);
    app.delete("/api/masterjenis/:id",[authJwt.verifyToken,authJwt.isAdmin],controller.delete);
}