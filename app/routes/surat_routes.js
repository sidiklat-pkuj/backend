const {authJwt} = require("../middleware");
const express = require('express');
const controller = require("../controllers/surat_controller");
const multer = require("multer");
const path = require("path");

module.exports=function(app){
    app.use(express.static('public'));
    const storageGambar = multer.diskStorage({
        destination: function (req,file,cb) {
                cb(null, './upload/file')
        },
        filename: (req, file, cb) => {
            return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
        }
    });
      var uploadGambar = multer({
        storage: storageGambar,
        limits: { fileSize: 10000000 }
    })
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.use('/api/file', express.static('./upload/file'));
    app.get("/api/surat/:menuid",[authJwt.verifyToken],controller.getByMenu);
    app.post("/api/surat",[authJwt.verifyToken],uploadGambar.single("file"),controller.tambah);
    app.put("/api/surat/:id",[authJwt.verifyToken],uploadGambar.single("file"),controller.edit);
    app.delete("/api/surat/:id",[authJwt.verifyToken],controller.delete);
}