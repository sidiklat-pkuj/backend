const {authJwt} = require("../middleware");
const controller = require("../controllers/menu_controller");

module.exports=function(app){
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.get("/api/menu/",[authJwt.verifyToken],controller.get);
    app.get("/api/menu/getpelatihan",[authJwt.verifyToken],controller.getTahunPelatihan);
    app.get("/api/menu/jenis/:jenis",[authJwt.verifyToken],controller.getByJenis);
    app.get("/api/menu/:id",[authJwt.verifyToken],controller.getOne);
    app.post("/api/menu",[authJwt.verifyToken,authJwt.isAdmin],controller.tambah);
    app.put("/api/menu/:id",[authJwt.verifyToken,authJwt.isAdmin],controller.edit);
    app.delete("/api/menu/:id",[authJwt.verifyToken,authJwt.isAdmin],controller.delete);
}