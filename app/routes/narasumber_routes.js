const {authJwt} = require("../middleware");
const express = require('express');
const controller = require("../controllers/narasumber_controller");
const multer = require("multer");
const path = require("path");

module.exports=function(app){
    app.use(express.static('public'));
    const storageGambar = multer.diskStorage({
        destination: function (req,file,cb) {
                cb(null, './upload/gambar')
        },
        filename: (req, file, cb) => {
            return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
        }
    });
      var uploadGambar = multer({
        storage: storageGambar,
        limits: { fileSize: 10000000 }
    })
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.use('/api/foto', express.static('./upload/gambar'));
    app.get("/api/narasumber/:submenuid",[authJwt.verifyToken],controller.getBySubMenu);
    app.post("/api/narasumber",[authJwt.verifyToken],uploadGambar.single("foto"),controller.tambah);
    app.put("/api/narasumber/:id",[authJwt.verifyToken],uploadGambar.single("foto"),controller.edit);
    app.delete("/api/narasumber/:id",[authJwt.verifyToken],controller.delete);
}