const {authJwt} = require("../middleware");
const controller = require("../controllers/sub_menu_controller");

module.exports=function(app){
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.get("/api/sub_menu",controller.get);
    app.post("/api/sub_menu",controller.tambah);
    app.put("/api/sub_menu/:id",controller.edit);
    app.delete("/api/sub_menu/:id",controller.delete);
}