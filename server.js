const express = require("express");
const cors = require("cors");

const app = express();


app.use(cors());

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// database
const db = require("./app/models");

db.sequelize.sync();

// routes
require('./app/routes/auth_routes')(app);
require('./app/routes/user_routes')(app);
require('./app/routes/role_routes')(app);
require('./app/routes/peserta_routes')(app);
require('./app/routes/menu_routes')(app);
require('./app/routes/sub_menu_routes')(app);
require('./app/routes/narasumber_routes')(app);
require('./app/routes/kurikulum_routes')(app);
require('./app/routes/mot_routes')(app);
require('./app/routes/dokumentasi_routes')(app);
require('./app/routes/esertifikat_routes')(app);
require('./app/routes/master_jenis_routes')(app);
require('./app/routes/surat_routes')(app);
require('./app/routes/keuangan_routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
